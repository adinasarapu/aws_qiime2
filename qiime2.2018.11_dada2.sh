#!/bin/sh

source activate qiime2-2018.11

# create a unique folder on the local compute drive
export TMPDIR=/tmp

if [ -e /bin/mktemp ]; then
  TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
  TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
  echo “Error. Cannot find mktemp to create tmp directory”
  exit
fi

# arguments passed through submit_ script
LEFT=$1 
RIGHT=$2 
TABLE=$3
META_FILE=$4
OUT_DIR=$5
THREADS=$6

chmod a+trwx $TMP_DIR

/usr/bin/mkdir -p $TMP_DIR/dada2

rsync -av $TABLE $TMP_DIR
rsync -av $META_FILE $TMP_DIR/meta_file.txt

DADA2_DIR=$TMP_DIR/dada2/trim_${LEFT}_${RIGHT}

echo "running.. qiime dada2 denoise-paired for ${LEFT}-${RIGHT} truncated reads"
qiime dada2 denoise-paired \
 --i-demultiplexed-seqs $TMP_DIR/paired_end_data.qza \
 --output-dir $DADA2_DIR \
 --p-n-threads $THREADS \
 --p-chimera-method pooled --p-min-fold-parent-over-abundance 1.0 \
 --p-hashed-feature-ids --p-max-ee 2.0 --p-trunc-q 2 --p-n-reads-learn 1000000 \
 --p-trim-left-f $LEFT \
 --p-trim-left-r $LEFT \
 --p-trunc-len-f $RIGHT \
 --p-trunc-len-r $RIGHT --verbose

# features observed in a user-defined fraction of the samples 
qiime feature-table core-features \
 --i-table $DADA2_DIR/table.qza \
 --p-min-fraction 0.5 \
 --p-max-fraction 1.0 \
 --p-steps 11 \
 --o-visualization $DADA2_DIR/table_core_features.qzv

qiime metadata tabulate \
 --m-input-file $TMP_DIR/meta_file.txt \
 --output-dir $DADA2_DIR/metadata
 
# Generates visual and tabular summaries of a feature table
qiime feature-table summarize \
 --i-table $DADA2_DIR/table.qza \
 --o-visualization $DADA2_DIR/table_summarize.qzv \
 --m-sample-metadata-file $TMP_DIR/meta_file.txt

# View sequence associated with each feature
qiime feature-table tabulate-seqs \
 --i-data $DADA2_DIR/representative_sequences.qza \
 --o-visualization $DADA2_DIR/representative_sequences.qzv

GROUP_TABLE=false

# Group samples or features in a feature table using metadata to define the mapping of IDs to a group.
# --p-mode [sum|median-ceiling|mean-ceiling]
# SubjectGroup  Batch   CaseControl
if [ "$GROUP_TABLE" = true ]; then
  qiime feature-table group \
   --i-table $DADA2_DIR/table.qza \
   --p-axis sample \
   --m-metadata-file $TMP_DIR/meta_file.txt \
   --m-metadata-column SubjectGroup \
   --p-mode median-ceiling \
   --o-grouped-table $DADA2_DIR/subject_grouped_table.qza

   # Generates visual and tabular summaries of a feature table
  qiime feature-table summarize \
   --i-table $DADA2_DIR/subject_grouped_table.qza \
   --o-visualization $DADA2_DIR/subject_grouped_table_summarize.qzv \
   --m-sample-metadata-file $TMP_DIR/meta_file.txt
fi

rsync -av $TMP_DIR/dada2 $OUT_DIR
/bin/rm -rf $TMP_DIR

source deactivate qiime2-2018.11

echo "Finish - `date`"
