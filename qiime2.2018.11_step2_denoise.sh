#!/bin/sh

#=======================================================#
# Written by						#
# Ashok R. Dinasarapu Ph.D, Emory University, Atlanta GA#
# Last updated on 12/14/2018				#
#=======================================================#

# Use this script to run qiime2.2018.11_dada2.sh
# Run batch by batch of samples and merge resulting tables using qiime2.2018.11_merge.sh
# Makesure to update DIR paths

PROJ_NAME=VFE11371

PROJ_DIR=$HOME/microbiome/$PROJ_NAME
QIIME2_DIR=$PROJ_DIR/qiime2_2018_11

META_FILE=$PROJ_DIR/${PROJ_NAME}.txt

DATA_DIR=$QIIME2_DIR/data_imported/BATCH1
TABLE=$DATA_DIR/paired_end_data.qza

OUT_DIR=$QIIME2_DIR/data_denoised/BATCH1

SCRIPT=$HOME/scripts/qiime2.2018.11/qiime2.2018.11_dada2.sh

# AWS Instance vCPUs: 16
# CPU options: Core count (8) x Threads per core (2) = Number of vCPUs 16 
 
THREADS=12

if [ ! -d $OUT_DIR ]; then
  mkdir -p $OUT_DIR
fi

for L in 20; do
 for R in 300; do
  $SCRIPT $L $R $TABLE $META_FILE $OUT_DIR $THREADS
 done
done
