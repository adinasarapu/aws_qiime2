#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D			#
# Emory University, Dept. of Human Genetics	#
# ashok.reddy.dinasarapu@emory.edu		#
# Date: 08/11/2018				#
#################################################

# 1. Phylogeny
# 2. Alpha and Beta rarefaction

# Is rarefaction essential when examining 16s amplicons from illumina NGS?
# Some diversity estimators can be rather sensitive to varying sample sizes, while others prove to be more robust

# https://usda-ars-gbru.github.io/Microbiome-workshop/tutorials/qiime2/

echo "Start - `date`" 
# /home/adinasarapu/microbiome/VFE11371/qiime2_2018_8/data_filtered/dada2/trim_20_245/subject_0

# number of threads (#$ -pe smp 9)
CORES=10
EXPERIMENT=VFE11371

# load module
source activate qiime2-2018.11

# create a TMP directory
export TMPDIR=/tmp
if [ -e /bin/mktemp ]; then
  TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
  TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
  echo “Error. Cannot find mktemp to create tmp directory”
  exit
fi

## PROJECT DIR
PROJ_DIR=$HOME/microbiome/$EXPERIMENT
QIIME2_DIR=$PROJ_DIR/qiime2_2018_11

SUBSET_NAME=subject_5
SAMPLING_MAX_DEPTH=25045

## INPUT DIR
# path to representative_sequences.qza 
DATA_DIR1=$QIIME2_DIR/data_filtered/dada2/trim_20_245/$SUBSET_NAME
# DATA_DIR1=$QIIME2_DIR/data_merged/dada2/trim_20_245
# path to filtered table.qza
DATA_DIR2=$QIIME2_DIR/data_filtered/dada2/trim_20_245/$SUBSET_NAME

## OUTPUT DIR
# local dir for final results
OUT_DIR=$QIIME2_DIR/data_rarefaction/dada2/trim_20_245/$SUBSET_NAME

# metadata file name
META_FILE=${EXPERIMENT}.txt
# metadata column name
METADATA_CATEGORY="SubjectGroup"

# copy metadata, feature table and sequences
rsync -av $PROJ_DIR/$META_FILE $TMP_DIR/meta_file.txt
rsync -av $DATA_DIR1/feature_frequency_filtered_rep_seq.qza $TMP_DIR/representative_sequences.qza
rsync -av $DATA_DIR2/feature_frequency_filtered_table.qza $TMP_DIR/filtered_table.qza

PHYLOGENY_DENOVO=true
ALPHA_RAREFACTION_ANALYSIS=true
BETA_RAREFACTION_ANALYSIS=true

ls -la $TMP_DIR/

#########################################################
# 1. Tree for phylogenetic diversity analyses         	#
# Needed for 						#
# 	Alpha_rarefaction and Alpha_phylogenetic 	#
# 	Beta_rarefaction and Beta_phylogenetic		#
#########################################################

if [ $PHYLOGENY_DENOVO = 'true' ]; then

  # create subdir in tmp
  /bin/mkdir -p $TMP_DIR/phylogeny

  # Many diversity analyses rely on the phylogenetic similarity between individual features. 
  # If you are sequencing phylogenetic markers (e.g., 16S rRNA genes), you can align these 
  # sequences to assess the phylogenetic relationship between each of your features.

  # Perform de novo multiple sequence alignment using MAFFT
  qiime alignment mafft \
   --i-sequences $TMP_DIR/representative_sequences.qza \
   --p-n-threads $CORES \
   --o-alignment $TMP_DIR/phylogeny/aligned_rep_seqs.qza

  # Export data from a QIIME 2 Artifact
  qiime tools export \
   --output-path $TMP_DIR/phylogeny/export/aligned --input-path $TMP_DIR/phylogeny/aligned_rep_seqs.qza
 
  # Mask (i.e., filter) unconserved and highly gapped columns from an alignment. 
  # These positions are generally considered to add noise to a resulting phylogenetic tree.
  qiime alignment mask \
   --i-alignment $TMP_DIR/phylogeny/aligned_rep_seqs.qza \
   --o-masked-alignment $TMP_DIR/phylogeny/masked_aligned_rep_seqs.qza

  # Use a multiple alignment viewer (https://www.ebi.ac.uk/Tools/msa/mview/)
  qiime tools export \
   --output-path $TMP_DIR/phylogeny/export/masked --input-path $TMP_DIR/phylogeny/masked_aligned_rep_seqs.qza
  
  # Construct a phylogenetic tree with RAxML
  #qiime phylogeny raxml \
  # --i-alignment $TMP_DIR/phylogeny/masked_aligned_rep_seqs.qza \
  # --p-seed 149 \
  # --p-n-searches 1 \
  # --p-n-threads $CORES \
  # --p-substitution-model GTRGAMMA \
  # --o-tree $TMP_DIR/phylogeny/unrooted_tree.qza  

  # Apply FastTree to generate a phylogenetic tree from the masked alignment.
  qiime phylogeny fasttree \
   --i-alignment $TMP_DIR/phylogeny/masked_aligned_rep_seqs.qza \
   --p-n-threads $CORES \
   --o-tree $TMP_DIR/phylogeny/unrooted_tree.qza

  # final step in this section we apply midpoint rooting
  qiime phylogeny midpoint-root \
   --i-tree $TMP_DIR/phylogeny/unrooted_tree.qza \
   --o-rooted-tree $TMP_DIR/phylogeny/rooted_tree.qza
  
  # Phylogenetic tree (newick) viewers 
  # (1. http://etetoolkit.org/treeview/ 2. http://www.trex.uqam.ca/index.php)
  qiime tools export \
   --output-path $TMP_DIR/phylogeny/export/rooted --input-path $TMP_DIR/phylogeny/rooted_tree.qza

fi

# The diversity in a single sample (alpha diversity) is commonly measured using metrics such as 
# the Shannon index and the Chao1 estimator, while the variation between pairs of samples (beta diversity) 
# is measured using metrics such as the Jaccard distance or Bray-Curtis dissimilarity. 

# Typically you want to choose a value high enough that you capture the diversity present in samples with high counts, 
# but low enough that you don’t get rid of a ton of your samples. 
# Given the the minimum count for any of your samples is 43253 (which is pretty high, and close to the maximum count in your samples), 
# you could probably just choose 43253 as your sampling depth.

# Many such metrics, including Shannon, Chao1, observed_otus, Jaccard and Bray-Curtis, are calculated from OTU frequencies.

# SAMPLING_MAX_DEPTH=24184

########################
# 2. Alpha Rarefaction #
########################

if [ $ALPHA_RAREFACTION_ANALYSIS = 'true' ]; then

  ALPHA_DIR=$TMP_DIR/alpha_rarefaction   
  /bin/mkdir -p $ALPHA_DIR

  ## ALPHA_RAREFACTION (shannon, chao1, faith_pd, ...)

  # alpha rarefaction curves by computing rarefactions between `min_depth` and `max_depth`
  
  qiime diversity alpha-rarefaction \
   --i-table $TMP_DIR/filtered_table.qza \
   --i-phylogeny $TMP_DIR/phylogeny/rooted_tree.qza \
   --p-max-depth $SAMPLING_MAX_DEPTH --p-min-depth 1 \
   --p-steps 10 --p-iterations 10 \
   --m-metadata-file $TMP_DIR/meta_file.txt \
   --p-metrics shannon \
   --p-metrics chao1 \
   --p-metrics faith_pd \
   --p-metrics observed_otus \
   --o-visualization $ALPHA_DIR/alpha_rarefaction.qzv \
   --verbose

  # richness estimator
  # --p-metrics chao1 \
fi

#######################
# 4. Beta Rarefaction #
#######################

if [ $BETA_RAREFACTION_ANALYSIS = 'true' ]; then

  BETA_DIR=$TMP_DIR/beta_rarefaction
  /bin/mkdir -p $BETA_DIR/{braycurtis,jaccard,unweighted_unifrac}
   
  ## BETA_RAREFACTION (spearman, jaccard, unweighted_unifrac, ...)

  # Repeatedly rarefy a feature table to compare beta diversity results across and within rarefaction depths
  # --p-iterations Number of times to rarefy the feature table at a given sampling depth

  qiime diversity beta-rarefaction \
   --i-table $TMP_DIR/filtered_table.qza \
   --i-phylogeny $TMP_DIR/phylogeny/rooted_tree.qza \
   --p-metric braycurtis \
   --p-sampling-depth $SAMPLING_MAX_DEPTH --p-iterations 10 \
   --p-correlation-method spearman \
   --p-clustering-method nj \
   --m-metadata-file $TMP_DIR/meta_file.txt \
   --o-visualization $BETA_DIR/braycurtis/beta_rarefaction.qzv \
   --verbose

  qiime diversity beta-rarefaction \
   --i-table $TMP_DIR/filtered_table.qza \
   --i-phylogeny $TMP_DIR/phylogeny/rooted_tree.qza \
   --p-metric jaccard \
   --p-sampling-depth $SAMPLING_MAX_DEPTH --p-iterations 10 \
   --p-correlation-method spearman \
   --p-clustering-method nj \
   --m-metadata-file $TMP_DIR/meta_file.txt \
   --o-visualization $BETA_DIR/jaccard/beta_rarefaction.qzv \
   --verbose

  qiime diversity beta-rarefaction \
   --i-table $TMP_DIR/filtered_table.qza \
   --i-phylogeny $TMP_DIR/phylogeny/rooted_tree.qza \
   --p-metric unweighted_unifrac \
   --p-sampling-depth $SAMPLING_MAX_DEPTH --p-iterations 10 \
   --p-correlation-method spearman \
   --p-clustering-method nj \
   --m-metadata-file $TMP_DIR/meta_file.txt \
   --o-visualization $BETA_DIR/unweighted_unifrac/beta_rarefaction.qzv \
  --verbose

fi

source deactivate qiime2-2018.11

/bin/rm $TMP_DIR/filtered_table.qza
/bin/rm $TMP_DIR/representative_sequences.qza

if [ ! -d $OUT_DIR ]; then
 mkdir -p $OUT_DIR
fi

rsync -av $TMP_DIR/* $OUT_DIR
/bin/rm -rf $TMP_DIR

echo "Finish - `date`"
