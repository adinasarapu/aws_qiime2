#!/bin/sh

# https://usda-ars-gbru.github.io/Microbiome-workshop/tutorials/qiime2/

#################################################
# Ashok R. Dinasarapu Ph.D			#
# Emory University, Dept. of Human Genetics	#
# ashok.reddy.dinasarapu@emory.edu		#
# Date: 12/09/2018				#
#################################################

echo "Start - `date`" 

# number of threads (#$ -pe smp 9)
CORES=5

EXPERIMENT=VFE11371
SUBSET_NAME=subject_5
SAMPLING_DEPTH=25045

# load module
source activate qiime2-2018.11

# create a TMP directory
export TMPDIR=/tmp
if [ -e /bin/mktemp ]; then
  TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
  TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
  echo “Error. Cannot find mktemp to create tmp directory”
  exit
fi

## PROJECT DIR
PROJ_DIR=$HOME/microbiome/$EXPERIMENT
QIIME2_DIR=$PROJ_DIR/qiime2_2018_11

## INPUT DIR
# path to representative_sequences.qza 
DATA_DIR1=$QIIME2_DIR/data_filtered/dada2/trim_20_245/$SUBSET_NAME
# path to filtered table.qza
DATA_DIR2=$QIIME2_DIR/data_filtered/dada2/trim_20_245/$SUBSET_NAME

## OUTPUT DIR
# local dir for final results
OUT_DIR=$QIIME2_DIR/data_taxonomy/dada2/trim_20_245/$SUBSET_NAME

# metadata file name
META_FILE=${EXPERIMENT}.txt
# metadata column name
# METADATA_CATEGORY="SubjectGroup"

echo -e "Copying metadata, table and sequences.. to $TMP_DIR"
rsync -av $PROJ_DIR/$META_FILE $TMP_DIR/meta_file.txt

rsync -av $DATA_DIR1/feature_frequency_filtered_rep_seq.qza $TMP_DIR/representative_sequences.qza
rsync -av $DATA_DIR2/feature_frequency_filtered_table.qza $TMP_DIR/filtered_table.qza

# always true
TAXONOMIC_COMPOSITION_ANALYSIS=true

# only one of the following should be true
SILVA_DB=true
GREEN_GENES_DB=false
HOMD_DB=false

ls -la $TMP_DIR/

#===========================#
# 1. Rarefy a feature table #
#===========================#
RAREFY_TABLE=true
if [ $RAREFY_TABLE = 'true' ]; then

  # Each column is subsampled to even depth without replacement (hypergeometric model)
  # Correction for unequal library sizes is applied

  echo -e "qiime feature-table rarefy with SAMPLING_DEPTH=$SAMPLING_DEPTH"
  qiime feature-table rarefy \
   --i-table $TMP_DIR/filtered_table.qza \
   --p-sampling-depth $SAMPLING_DEPTH \
   --o-rarefied-table $TMP_DIR/filtered_rarefied_table.qza
  
fi

#===============================================================================================================#
# 1. Taxonomic analysis - 											#
#    Taxonomic classification of sequences (a.k.a., “what species are present?”)				#
# 2. Differential abundance analysis - 										#
#    which features (OTUs,ASVs or taxa) are significantly more/less abundant in different experimental groups  	#					
#===============================================================================================================#

if [ $TAXONOMIC_COMPOSITION_ANALYSIS = 'true' ]; then
  
  # create sub dir in tmp
  /bin/mkdir -p $TMP_DIR/{taxonomy,composition}

  if [ $GREEN_GENES_DB = 'true' ]; then
 
    TAX_DIR=$TMP_DIR/taxonomy/GGENES
    COM_DIR=$TMP_DIR/composition/GGENES
   
    IDENTITY=0.8

   # GREENGENES DATABASE #
   #=====================#
   # The current reference data (Greengenes 13_8) files are:
   # reference sequences: 97% OTU representative sequences (gg_13_8_otus/rep_set/97_otus.fasta)
   # reference taxonomy: 97% OTU representative sequence taxonomic assignments (gg_13_8_otus/taxonomy/97_otu_taxonomy.txt) 
    echo -e "Greengenes 13_8, 97% OTU representative sequences and taxonomy"
    rsync -av $HOME/DB/gg_13_8_otus/rep_set/99_otus.fasta $TMP_DIR/db_otus.fasta
    rsync -av $HOME/DB/gg_13_8_otus/taxonomy/99_otu_taxonomy.txt $TMP_DIR/db_otu_taxonomy.txt

  fi
  
  if [ $HOMD_DB = 'true' ]; then

    TAX_DIR=$TMP_DIR/taxonomy/HOMD
    COM_DIR=$TMP_DIR/composition/HOMD
    
    IDENTITY=0.8

    #=================================================#
    # expanded Human Oral Microbiome Database (eHOMD) #
    #=================================================#
    # http://www.ehomd.org/index.php?name=seqDownload&file&type=R
    rsync -av $HOME/DB/HOMD_16S_rRNA_RefSeq/HOMD_16S_rRNA_RefSeq_V15.1.fasta $TMP_DIR/db_otus.fasta
    rsync -av $HOME/DB/HOMD_16S_rRNA_RefSeq/HOMD_16S_rRNA_RefSeq_V15.1.qiime.taxonomy $TMP_DIR/db_otu_taxonomy.txt
  fi

  if [ $SILVA_DB = 'true' ]; then
    
    TAX_DIR=$TMP_DIR/taxonomy/SILVA
    COM_DIR=$TMP_DIR/composition/SILVA

    IDENTITY=0.9

    #==============#
    # SILVA 132 DB #
    #==============# 
    rsync -av $HOME/DB/SILVA_132_QIIME_release/rep_set/rep_set_16S_only/99/silva_132_99_16S.fna $TMP_DIR/db_otus.fasta
    rsync -av $HOME/DB/SILVA_132_QIIME_release/taxonomy/16S_only/99/taxonomy_7_levels.txt $TMP_DIR/db_otu_taxonomy.txt

  fi

  if [ ! -d $TAX_DIR ]; then  
    /bin/mkdir -p $TAX_DIR
    echo "cretaed $TAX_DIR"
  fi

  if [ ! -d $COM_DIR ]; then
    /bin/mkdir -p $COM_DIR
    echo "cretaed $COM_DIR"
  fi

  # Extracting ref reads using 16S rRNA gene-specific primers
  # 515F PCR Primer: GTGCCAGCMGCCGCGGTAA and 806r PCR Primer: GGACTACHVGGGTWTCTAAT
  # 341F 5'-CCTACGGGNGGCWGCAG-3' and 805R 5'-GACTACHVGGGTATCTAATCC-3' 

  fprimer="CCTACGGGNGGCWGCAG"
  rprimer="GACTACHVGGGTATCTAATCC"

  qiime tools import \
   --type 'FeatureData[Sequence]' \
   --input-path $TMP_DIR/db_otus.fasta \
   --output-path $TAX_DIR/db_otus.qza

  qiime tools import \
   --type 'FeatureData[Taxonomy]' \
   --input-format HeaderlessTSVTaxonomyFormat \
   --input-path $TMP_DIR/db_otu_taxonomy.txt \
   --output-path $TAX_DIR/ref_taxonomy.qza

  # 0.9 for silva; 0.8 for other
  echo "Extract sequencing-like reads from a reference database "
  qiime feature-classifier extract-reads \
   --i-sequences $TAX_DIR/db_otus.qza \
   --p-f-primer $fprimer \
   --p-r-primer $rprimer \
   --p-identity $IDENTITY \
   --p-trunc-len 245 \
   --p-trim-left 20 \
   --o-reads $TAX_DIR/ref_seqs.qza \
   --verbose

  echo "Creating a scikit-learn naive_bayes classifier for reads"
  qiime feature-classifier fit-classifier-naive-bayes \
   --i-reference-reads $TAX_DIR/ref_seqs.qza \
   --i-reference-taxonomy $TAX_DIR/ref_taxonomy.qza \
   --o-classifier $TAX_DIR/classifier.qza

  echo "Classifying reads by taxon using a fitted classifier"

  # the confidence is related to how many times the subsample (of sequences) comes up with the same classification
  # The confidence value can’t be directly translated into a percent identity, though in general a high confidence 
  # at a lower taxonomy level (e.g., species) is probably indicative of a high percent identity database match. 
  # However, it is also possible to have a high percent identity match but a low confidence, if the sequence that 
  # you matched is identical across several different taxonomic groups.

  qiime feature-classifier classify-sklearn \
   --i-classifier $TAX_DIR/classifier.qza \
   --i-reads $TMP_DIR/representative_sequences.qza \
   --p-n-jobs $CORES \
   --p-confidence 0.7 \
   --o-classification $TAX_DIR/taxonomy.qza
  
  # Generate a tabular view of Metadata
  qiime metadata tabulate \
   --m-input-file $TAX_DIR/taxonomy.qza \
   --o-visualization $TAX_DIR/taxonomy.qzv

  # Export taxonomy data to tabular format
  qiime tools export \
   --output-path $TAX_DIR/export \
   --input-path $TAX_DIR/taxonomy.qza

  # ============================#
  # Filter feature contaminants #
  #=============================#
  #(https://usda-ars-gbru.github.io/Microbiome-workshop/tutorials/qiime2/)
  
  # search for matching lines with grep then select the id column
  # -i, --ignore-case; 
  # -v --invert-match
  grep -i "mitochondia|chloroplast|Feature" $TAX_DIR/export/taxonomy.tsv | \
   cut -f 1 > $TAX_DIR/export/chloro_mito_ids.txt
   
  /bin/mkdir -p $TAX_DIR/filtered

  echo "Filtering taxa contaminants from table, like mitochondria,chloroplast"
  qiime taxa filter-table \
   --i-table $TMP_DIR/filtered_rarefied_table.qza \
   --i-taxonomy $TAX_DIR/taxonomy.qza \
   --p-exclude mitochondria,chloroplast,Feature \
   --o-filtered-table $TAX_DIR/filtered/chloro_mito_filtered_table.qza

  # Export data to biom format
  qiime tools export \
   --output-path $TAX_DIR/filtered/export \
   --input-path $TAX_DIR/filtered/chloro_mito_filtered_table.qza

  echo "Filtering taxa contaminants from sequences, like mitochondria,chloroplast"
  qiime taxa filter-seqs \
   --i-sequences $TMP_DIR/representative_sequences.qza \
   --i-taxonomy $TAX_DIR/taxonomy.qza \
   --p-exclude mitochondria,chloroplast,Feature \
   --o-filtered-sequences $TAX_DIR/filtered/chloro_mito_filtered_sequences.qza
  
  # view the taxonomic composition of our samples with interactive bar plots
  # correction for unequal library sizes is applied through table rarefy
  
  echo "qiime taxa barplot"
  qiime taxa barplot \
   --i-table $TAX_DIR/filtered/chloro_mito_filtered_table.qza \
   --i-taxonomy $TAX_DIR/taxonomy.qza \
   --m-metadata-file $TMP_DIR/meta_file.txt \
   --o-visualization $TAX_DIR/filtered/taxa_bar_plots.qzv

  #==========================================================#
  # make rarefied taxonomy table at genus (taxonomic) level  #
  #==========================================================#

  # taxa collapsed
  #declare -A LEVEL_MAP
  #LEVEL_MAP[1]=kingdom # (e.g Bacteria)
  #LEVEL_MAP[2]=phylum # (e.g Actinobacteria)
  #LEVEL_MAP[3]=class # (e.g Actinobacteria)
  #LEVEL_MAP[4]=order # (e.g Actinomycetales)
  #LEVEL_MAP[5]=family # (e.g Streptomycetaceae)
  #LEVEL_MAP[6]=genus # (e.g Streptomyces)
  #LEVEL_MAP[7]=species # (e.g mirabilis)

  #taxa_level=6
  #taxa_name=${LEVEL_MAP[$taxa_level]}

  #================================================#
  # Analysis of Composition of Microbiomes (ANCOM) #
  #================================================#
  # ANCOM uses the Mann-Whitney test to avoid any distributional assumptions
  # Analysis of composition of microbiomes—compares the log ratio of the abundance of each taxon 
  # to the abundance of all the remaining taxa one at a time. The Mann-Whitney U is then calculated 
  # on each log ratio

  #filter based on body site
  qiime feature-table filter-samples \
   --i-table $TAX_DIR/filtered/chloro_mito_filtered_table.qza \
   --m-metadata-file $TMP_DIR/meta_file.txt \
   --p-where "SubjectGroup='STOOL'" \
   --o-filtered-table $TAX_DIR/filtered/stool_filtered_table.qza

  qiime taxa collapse \
   --i-table $TAX_DIR/filtered/stool_filtered_table.qza \
   --i-taxonomy $TAX_DIR/taxonomy.qza \
   --p-level 6 \
   --o-collapsed-table $TAX_DIR/filtered/stool_level6_table.qza

  qiime composition add-pseudocount \
   --i-table $TAX_DIR/filtered/stool_level6_table.qza \
   --p-pseudocount 1 \
   --o-composition-table $TAX_DIR/filtered/stool_composition_table.qza

  # Apply ANCOM to identify features that are differentially abundant across groups
  qiime composition ancom \
   --i-table $TAX_DIR/filtered/stool_composition_table.qza \
   --m-metadata-file $TMP_DIR/meta_file.txt \
   --p-transform-function log \
   --p-difference-function mean_difference \
   --m-metadata-column CaseControl2 \
   --o-visualization $COM_DIR/stool_ancom_group.qzv

  #filter based on body site
  qiime feature-table filter-samples \
   --i-table $TAX_DIR/filtered/chloro_mito_filtered_table.qza \
   --m-metadata-file $TMP_DIR/meta_file.txt \
   --p-where "SubjectGroup='ASCENDING'" \
   --o-filtered-table $TAX_DIR/filtered/ascending_filtered_table.qza

  qiime taxa collapse \
   --i-table $TAX_DIR/filtered/ascending_filtered_table.qza \
   --i-taxonomy $TAX_DIR/taxonomy.qza \
   --p-level 6 \
   --o-collapsed-table $TAX_DIR/filtered/ascending_level6_table.qza

  qiime composition add-pseudocount \
   --i-table $TAX_DIR/filtered/ascending_level6_table.qza \
   --p-pseudocount 1 \
   --o-composition-table $TAX_DIR/filtered/ascending_composition_table.qza
 
  # Apply ANCOM to identify features that are differentially abundant across groups
  qiime composition ancom \
   --i-table $TAX_DIR/filtered/ascending_composition_table.qza \
   --m-metadata-file $TMP_DIR/meta_file.txt \
   --p-transform-function log \
   --p-difference-function mean_difference \
   --m-metadata-column CaseControl2 \
   --o-visualization $COM_DIR/ascending_ancom_group.qzv

  #filter based on body site
  qiime feature-table filter-samples \
   --i-table $TAX_DIR/filtered/chloro_mito_filtered_table.qza \
   --m-metadata-file $TMP_DIR/meta_file.txt \
   --p-where "SubjectGroup='RECTUM'" \
   --o-filtered-table $TAX_DIR/filtered/rectum_filtered_table.qza
 
  qiime taxa collapse \
   --i-table $TAX_DIR/filtered/rectum_filtered_table.qza \
   --i-taxonomy $TAX_DIR/taxonomy.qza \
   --p-level 6 \
   --o-collapsed-table $TAX_DIR/filtered/rectum_level6_table.qza

  qiime composition add-pseudocount \
   --i-table $TAX_DIR/filtered/rectum_level6_table.qza \
   --p-pseudocount 1 \
   --o-composition-table $TAX_DIR/filtered/rectum_composition_table.qza
    
  # Apply ANCOM to identify features that are differentially abundant across groups
  qiime composition ancom \
   --i-table $TAX_DIR/filtered/rectum_composition_table.qza \
   --m-metadata-file $TMP_DIR/meta_file.txt \
   --p-transform-function log \
   --p-difference-function mean_difference \
   --m-metadata-column CaseControl2 \
   --o-visualization $COM_DIR/rectum_ancom_group.qzv
  
  #filter based on body site
  qiime feature-table filter-samples \
   --i-table $TAX_DIR/filtered/chloro_mito_filtered_table.qza \
   --m-metadata-file $TMP_DIR/meta_file.txt \
   --p-where "SubjectGroup='SIGMOID'" \
   --o-filtered-table $TAX_DIR/filtered/sigmoid_filtered_table.qza

  qiime taxa collapse \
   --i-table $TAX_DIR/filtered/sigmoid_filtered_table.qza \
   --i-taxonomy $TAX_DIR/taxonomy.qza \
   --p-level 6 \
   --o-collapsed-table $TAX_DIR/filtered/sigmoid_level6_table.qza
 
  qiime composition add-pseudocount \
   --i-table $TAX_DIR/filtered/sigmoid_level6_table.qza \
   --p-pseudocount 1 \
   --o-composition-table $TAX_DIR/filtered/sigmoid_composition_table.qza
    
  # Apply ANCOM to identify features that are differentially abundant across groups
  qiime composition ancom \
   --i-table $TAX_DIR/filtered/sigmoid_composition_table.qza \
   --m-metadata-file $TMP_DIR/meta_file.txt \
   --p-transform-function log \
   --p-difference-function mean_difference \
   --m-metadata-column CaseControl2 \
   --o-visualization $COM_DIR/sigmoid_ancom_group.qzv

fi

source deactivate qiime2-2018.11

/bin/rm $TMP_DIR/filtered_table.qza
/bin/rm $TMP_DIR/representative_sequences.qza
/bin/rm $TMP_DIR/db_otus.fasta
/bin/rm $TMP_DIR/db_otu_taxonomy.txt

if [ ! -d $OUT_DIR ]; then
 mkdir -p $OUT_DIR
fi

rsync -av $TMP_DIR/* $OUT_DIR
/bin/rm -rf $TMP_DIR

echo "Finish - `date`"
