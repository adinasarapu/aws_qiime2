#!/bin/sh

# Run this script to merge two ASV tables (out put from step2) 
# https://forum.qiime2.org/t/filtering-and-then-merging-2-different-runs-same-barcodes-used-in-both-runs/897

echo "Start - `date`" 

# trim_0_245
# trim_0_250
# trim_0_300

# trim_20_245
# trim_20_250
# trim_20_300

PROJ_NAME=VFE11371
TRIM=trim_20_245

source activate qiime2-2018.11

PROJ_DIR=$HOME/microbiome/$PROJ_NAME
QIIME2_DIR=$PROJ_DIR/qiime2_2018_11

META_FILE=${PROJ_NAME}.txt
TABLES_MERGE=true
SEQS_MERGE=true

# error_on_overlapping_sample|error_on_overlapping_feature|sum
OVERLAP_METHOD=error_on_overlapping_sample

export TMPDIR=/tmp

if [ -e /bin/mktemp ]; then
 TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

chmod a+trwx $TMP_DIR

MERGED_DIR=$QIIME2_DIR/data_merged

rsync -av $PROJ_DIR/$META_FILE $TMP_DIR/meta_file.txt

# DADA2 table and seq file paths

TABLE_1=$QIIME2_DIR/data_denoised/BATCH1/dada2/$TRIM/table.qza
TABLE_2=$QIIME2_DIR/data_denoised/BATCH2/dada2/$TRIM/table.qza

SEQ_1=$QIIME2_DIR/data_denoised/BATCH1/dada2/$TRIM/representative_sequences.qza
SEQ_2=$QIIME2_DIR/data_denoised/BATCH2/dada2/$TRIM/representative_sequences.qza

OUT_DIR=$MERGED_DIR/dada2/$TRIM

# merge - combine multiple tables
if [ "$TABLES_MERGE" = true ]; then

 rsync -av $TABLE_1 $TMP_DIR/table1.qza
 rsync -av $TABLE_2 $TMP_DIR/table2.qza

 # Combines feature tables using the `overlap_method` provided 
 qiime feature-table merge \
  --i-tables $TMP_DIR/table1.qza \
  --i-tables $TMP_DIR/table2.qza \
  --p-overlap-method $OVERLAP_METHOD \
  --o-merged-table $TMP_DIR/merged_table.qza

 # Summarize table
 qiime feature-table summarize \
  --i-table $TMP_DIR/merged_table.qza \
  --o-visualization $TMP_DIR/table_summarize.qzv \
  --m-sample-metadata-file $TMP_DIR/meta_file.txt
fi

if [ "$SEQS_MERGE" = true ]; then

 rsync -av $SEQ_1 $TMP_DIR/rep_seq1.qza
 rsync -av $SEQ_2 $TMP_DIR/rep_seq2.qza

 qiime feature-table merge-seqs \
  --i-data $TMP_DIR/rep_seq1.qza \
  --i-data $TMP_DIR/rep_seq2.qza \
  --o-merged-data $TMP_DIR/merged_seqs.qza

 # View sequences associated with each feature
 qiime feature-table tabulate-seqs \
  --i-data $TMP_DIR/merged_seqs.qza \
  --o-visualization $TMP_DIR/merged_seqs.qzv
fi

# Convert frequencies to relative frequencies by 
# dividing each frequency in a sample by the sum of frequencies in that sample

qiime feature-table relative-frequency \
 --i-table $TMP_DIR/merged_table.qza \
 --o-relative-frequency-table $TMP_DIR/rel_frequency_merged_table.qza

qiime feature-table summarize \
 --i-table $TMP_DIR/rel_frequency_merged_table.qza \
 --o-visualization $TMP_DIR/rel_frequency_table_summarize.qzv \
 --m-sample-metadata-file $TMP_DIR/meta_file.txt

/bin/rm $TMP_DIR/*1.qza
/bin/rm $TMP_DIR/*2.qza
 
if [ ! -d $OUT_DIR ]; then
 mkdir -p $OUT_DIR
fi
 
rsync -av $TMP_DIR/* $OUT_DIR
/bin/rm -rf $TMP_DIR

source deactivate qiime2-2018.11

echo "Finish - `date`"
