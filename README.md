# 16S rRNA Data Analysis Using QIIME2-2018.11 at AWS Cloud 

The following pipeline is intended to help our collaborators in analysis of 16S rRNA sequencing data via QIIME2 (2018.11).

## 1. `Sign In` to the AWS Console

[https://aws.amazon.com/console/](https://aws.amazon.com/console/)

## 2. Go to Images, select AMI QIIME2-2018.11 and launch it!

For new installation, instructions are available at [https://docs.qiime2.org](https://docs.qiime2.org/2018.6/install/)

## 3. Choose an Instance Type

For example,

`Family Type:` General purpose
`Type:` m5a.4xlarge
`vCPUs:` 16 (see CPU options)
`Memory (GiB):` 64
`Instance Storage (GB):` EBS only
`EBS-Optimized Available:` Yes
`Network Performance:` Up to 10 Gigabit
`IPv6 Support:` Yes

## 4. Configure Instance Details

Configure the instance to suit your requirements. You can launch multiple instances from the same AMI.

For example,

`Number of instances:` 1
`Network:` default-VPC
`Subnet:`select one of the exising subnets 
`Auto-assign Public IP:` Enable
`CPU options:` Core count (8) x Threads per core (2) = Number of vCPUs 16 
`Shutdown behavior:` Stop
`Tenancy:` Shared – run a shared hardware instance

## 5. Add Storage

You can also attach additional EBS volumes after launching an instance, but not instance store volumes.

For example,

`Size (GiB):` 100
`Volume Type:` General Purpose (SSD)
`Delete on Termination:` yes

## 6. Add Tags

A tag consists of a case-sensitive key-value pair. A copy of a tag can be applied to volumes, instances or both.

For example,

`Key:` Microbiome
`Value:` QIIME2.2018.11

## 7. Configure Security Group

A security group is a set of firewall rules that control the traffic for your instance. On this page, you can add rules to allow specific traffic to reach your instance. You can create a new security group or select from an existing one below.

`Select an existing security group:` launch-wizard (associates with VPC selection)

## 8. Review Instance Launch

`Choose an existing key pair` 
`Select a key pair:`  (previously downloaded `xxx.pem` file)

## 9. Login EC2 Instance/EBS using AWS Cli

Get Public DNS by selecting the launched EC2 instance. 

`chmod 400 xxx.pem`

`ssh -i xxx.pem ec2-user@includePublicDNS` 

## 10. Copy Data from local computer to AWS

`scp -i xxx.pem -r dataDir ec2-user@includePublicDNS:~/`

Along with raw/fastq data, download and copy one of the following databases 

1. [SILVA](https://www.arb-silva.de/download/archive/qiime/)
2. [GREENGENES](http://greengenes.secondgenome.com)
3. [expanded Human Oral Microbiome Database](http://www.homd.org/index.php)

## 11. Run [scripts](https://bitbucket.org/adinasarapu/aws_qiime2/src)

Use screen command to get in/out of the system while keeping the processes running. Since there are 16 threads available for current instance. Run two processes each with 8 threads.

`screen -S screenName`

if you want to exit the terminal without killing the running process, simply press `Ctrl+A+D`

To reconnect to the screen: `screen -R screenName`

`echo $STY` [current screen]

`screen –ls` lists all screens
