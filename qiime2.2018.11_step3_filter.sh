#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D                      #
# Emory University, Dept. of Human Genetics     #
# ashok.reddy.dinasarapu@emory.edu              #
# Last updated on 12/14/2018                              #
#################################################

# 1. Feature (and Sample) filtering
# 2. Sample subset based on metadata file

ID=5

echo "Start - `date`" 

source activate qiime2-2018.11

TRIM=trim_20_245
# ID=1 # 0-9
SUBSET_NAME=subject_${ID}

EXPERIMENT=VFE11371

# SAMPLE_METADATA
META_FILE="${EXPERIMENT}.txt"

#META_DATA_COLUMN="SubjectGroup" 

PROJ_DIR=$HOME/microbiome/$EXPERIMENT
QIIME2_DIR=$PROJ_DIR/qiime2_2018_11

DATA_DIR=$QIIME2_DIR/data_merged/dada2/$TRIM
OUT_DIR=$QIIME2_DIR/data_filtered/dada2/$TRIM/$SUBSET_NAME

export TMPDIR=/tmp

if [ -e /bin/mktemp ]; then
 TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

# copy 
# table.qza
rsync -av $DATA_DIR/merged_table.qza $TMP_DIR/table.qza
rsync -av $DATA_DIR/merged_seqs.qza $TMP_DIR/representative_sequences.qza
# meta data file 
rsync -av $PROJ_DIR/$META_FILE $TMP_DIR/meta_file.txt

######################################################################
# Filter (i.e., remove) samples and/or features from a feature table #
######################################################################
SINGLETONS_FILTER=true
FEATURE_FREQ_FILTER=true
SAMPLE_FILTER=true

#===================#
# Feature filtering #
#===================#
#Filter features from table based on frequency and/or metadata.

# Contingency-based filtering
# Features that are present in only a single sample could be filtered from a feature table as follows
# Removes singletons
if [ "$SINGLETONS_FILTER" = true ]; then
  
  # filter table
  qiime feature-table filter-features \
   --i-table $TMP_DIR/table.qza \
   --p-min-samples 2 \
   --o-filtered-table $TMP_DIR/singletons_filtered_table.qza

  # visualize/summary table
  qiime feature-table summarize \
   --i-table $TMP_DIR/singletons_filtered_table.qza \
   --o-visualization $TMP_DIR/singletons_filtered_table.qzv \
   --m-sample-metadata-file $TMP_DIR/meta_file.txt

  # filter seq
  qiime feature-table filter-seqs \
   --i-table $TMP_DIR/singletons_filtered_table.qza \
   --i-data $TMP_DIR/representative_sequences.qza \
   --o-filtered-data $TMP_DIR/singletons_filtered_rep_seq.qza

fi

if [ "$FEATURE_FREQ_FILTER" = true ]; then
 
   # Remove features with a total abundance (summed across all samples) of < 50 as follows
  qiime feature-table filter-features \
   --i-table $TMP_DIR/table.qza \
   --p-min-frequency 10 \
   --o-filtered-table $TMP_DIR/feature_frequency_filtered_table.qza

  # visualize/summary table
  qiime feature-table summarize \
   --i-table $TMP_DIR/feature_frequency_filtered_table.qza \
   --o-visualization $TMP_DIR/feature_frequency_filtered_table.qzv \
   --m-sample-metadata-file $TMP_DIR/meta_file.txt
 
  # filter seq 
  qiime feature-table filter-seqs \
   --i-table $TMP_DIR/feature_frequency_filtered_table.qza \
   --i-data $TMP_DIR/representative_sequences.qza \
   --o-filtered-data $TMP_DIR/feature_frequency_filtered_rep_seq.qza

fi

# Sample subset
if [ "$SAMPLE_FILTER" = true ]; then
  
  # Filter samples from table based on frequency and/or metadata
  #qiime feature-table filter-samples \
  # --i-table $TMP_DIR/feature_frequency_filtered-table.qza \
  # --p-min-frequency 90 \
  # --o-filtered-table $TMP_DIR/sample_filtered_table.qza

 # --p-where "Experiment='$EXPERIMENT'"
 # --p-where "BodySite='gut' AND Year='2008' AND Subject='subject-1'"
 # SubjectGroup:STOOL,ASCENDING,RECTUM,SIGMOID   CaseControl:0,1
 qiime feature-table filter-samples \
  --i-table $TMP_DIR/table.qza \
  --m-metadata-file $TMP_DIR/meta_file.txt \
  --p-where "SubjectID='$ID'" \
  --o-filtered-table $TMP_DIR/${SUBSET_NAME}_subset_table.qza

 #create filtered representative sequences based on new subset filtered table
 qiime feature-table filter-seqs \
  --i-table $TMP_DIR/${SUBSET_NAME}_subset_table.qza \
  --i-data $TMP_DIR/representative_sequences.qza \
  --o-filtered-data $TMP_DIR/${SUBSET_NAME}_subset_rep_seq.qza

 qiime feature-table summarize \
  --i-table $TMP_DIR/${SUBSET_NAME}_subset_table.qza \
  --o-visualization $TMP_DIR/${SUBSET_NAME}_subset_table.qzv \
  --m-sample-metadata-file $TMP_DIR/meta_file.txt

# qiime feature-table heatmap \
#  --i-table $TMP_DIR/sample-subset-filtered-table.qza \
#  --m-metadata-file $TMP_DIR/meta_file.txt \
#  --p-metric euclidean \
#  --p-cluster both \
#  --p-method average \
#  --m-metadata-column $META_DATA_COLUMN \
#  --o-visualization $TMP_DIR/sample-subset-filtered-table_heatmap.qzv
fi

# qiime feature-table rarefy 

source deactivate qiime2-2018.11

/bin/rm $TMP_DIR/table.qza


if [ ! -d $OUT_DIR ]; then
  mkdir -p $OUT_DIR
fi

rsync -av $TMP_DIR/* $OUT_DIR
/bin/rm -rf $TMP_DIR

echo "Finish - `date`"
